const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// creating the schema
const postSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  content: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  image: {
    type: String,
    required: true
  }
});

const post = (module.exports = mongoose.model("post", postSchema));
