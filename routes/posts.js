const express = require("express");
const router = express.Router();
const multer = require("multer");
const Post = require("../models/posts");

// multer middleware
// Multer middleware
const fileFilter = (req, file, cb) => {
  if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
    cb(null, true);
  } else {
    cb(null, false);
  }
};
const upload = multer({
  dest: "uploads/",
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

// count posts
router.get("/api/getPostCount", (req, res) => {
  Post.find({}).then(data => {
    res.send({ success: 1, data });
  });
});
router.get("/api/getPosts", (req, res) => {
  console.log(req.query);
  const pageSize = +req.query.pageSize;
  // parseInt(pageSize);
  const currentPage = +req.query.currentPage;
  const postQuery = Post.find({});

  if (pageSize && currentPage) {
    postQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
  }
  postQuery

    .then(posts => {
      res.json({ message: "sucess", posts });
    })
    .catch(err => {
      res.send(err);
    });
});

router.post("/api/addPost", upload.single("image"), (req, res) => {
  const url = req.protocol + "://" + req.get("host");
  console.log(req.body);
  // const post = req.body;
  const post = new Post({
    title: req.body.title,
    content: req.body.content,
    image: url + "/uploads/" + req.file.filename
  });
  console.log(post);
  post
    .save()
    .then(data => {
      console.log(data, "testing purpose");
      res.status(201).send({ message: "post added successfully", post: data });
    })
    .catch(err => {
      console.log(err);
    });
  console.log(post);
});

router.delete("/api/deletePost/:id", (req, res) => {
  Post.deleteOne({ _id: req.params.id })
    .then(data => {
      res.send({ message: "Deleted Successfully", data });
    })
    .catch(err => {
      res.send({ message: "Not Deleted Successfully", err });
    });
});

router.put("/api/updatePost/:id", upload.single("image"), (req, res) => {
  let imagePath = req.body.image;
  if (req.file) {
    const url = req.protocol + "://" + req.get("host");
    imagePath = url + "/uploads/" + req.file.filename;
  }
  console.log(req.params, req.body, req.file, imagePath);

  // const post = new Post({
  //   _id: req.params.id,
  //   title: req.body.title,
  //   content: req.body.content
  // });
  Post.findOne({ _id: req.params.id })
    .then(post => {
      post.title = req.body.title;
      post.content = req.body.content;
      post.image = imagePath;

      post.save().then(post => {
        res.send({ message: "Successfully Edited", post });
      });
    })
    .catch(err => {
      console.log(err);
    });
});

router.get("/api/getSinglePost/:id", (req, res) => {
  Post.findOne({ _id: req.params.id })
    .then(post => {
      if (post) {
        res.send({ message: "Post fetched successfully", post });
      } else {
        res.send({ message: "Post not found" });
      }
    })
    .catch(err => {
      console.log(err);
    });
});
module.exports = router;
