const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const multer = require("multer");
const path = require("path");
const app = express();

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Credentials", "true");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET,HEAD,OPTIONS,POST,PUT,DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
  );
  next();
});

// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//
app.use("/uploads", express.static(path.join("uploads")));

// Posts api route
const postsroute = require("./routes/posts");
app.use("/", postsroute);

// cors middleware
app.use(cors({ origin: true }));

mongoose.Promise = global.Promise;

// Connecting to mongoose database
mongoose
  .connect(
    "mongodb+srv://dharmik:dharmik@cluster0-ntfof.mongodb.net/node-angular?retryWrites=true&w=majority",
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log("MongoDB connected");
  })
  .catch(err => console.log(err));

//   Local port
const port = process.env.PORT || 5000;
app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});

module.exports = app;
