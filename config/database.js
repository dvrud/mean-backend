if (process.env.NODE_ENV === "production") {
  module.exports = {
    mongoURI: "mongodb://dharmik:dharmik1@ds239967.mlab.com:39967/hymns"
  };
} else {
  module.exports = { mongoURI: "mongodb://localhost/essays" };
}
